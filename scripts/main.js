$(document).ready(function(){
    $("#message").focus(function(){
        $("#lblMessage").removeClass("label-message");
        $("#lblMessage").addClass("label-focus");
    });

    $("#message").blur(function(){
        if($("#message").val().length <= 0){
            $("#lblMessage").removeClass("label-focus");
            $("#lblMessage").addClass("label-message");
        }
    });

    $("#message").keyup(function(){
        if($("#message").val().length > 0){
            $("#submit").removeClass("btn btn-secondary");
            $("#submit").addClass("btn btn-primary");
            $("#submit").prop('disabled', false);
        }
        else {
            $("#submit").removeClass("btn btn-primary");
            $("#submit").addClass("btn btn-secondary");
            $("#submit").prop('disabled', true);
        }
    });
});